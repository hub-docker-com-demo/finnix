# finnix

Finnix is a self-contained, bootable Linux CD distribution ("LiveCD") for system administrators, based on Debian.

(Unofficial demo and howto)

## Links
* https://www.finnix.org/
* https://hub.docker.com/r/ppc32/finnix/
* https://github.com/travis-util/qemu

## TODO
* Debug Finnix part maybe sending arrow special char from python pexpect.